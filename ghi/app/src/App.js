import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import { Fragment } from 'react';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './new-conference'
import AttendeeForm from './attend-conference';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {Routes, Route, BrowserRouter} from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="attendees">
              <Route index element={<AttendeesList attendees={props.attendees} />} />
              <Route path="new" element={<AttendeeForm />} />
            </Route>
            <Route path="presentation">
              <Route path="new" element={<PresentationForm />} />
            </Route>
          </Routes>
  </BrowserRouter>
  );
};

export default App;
