import { NavLink } from "react-router-dom";



function Nav() {
    return (
        <header>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference GO!</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                    <NavLink className="nav-link" aria-current="page" to="/locations/new">New Location</NavLink>
                    <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
                    <NavLink className="nav-link" aria-current="page" to="/presentation/new">New Presentation</NavLink>
                </ul>
                <form className="d-flex">
                    <input className="form-control me-2" type="search" placeholder="Search conferences" aria-label="Search"></input>
                    <button className="btn btn-outline-success" type="submit">Search</button>
                    <NavLink className="btn btn-primary" to="/attendees/new">Attend!</NavLink>
                </form>
                </div>
            </div>
            </nav>
        </header>
);
}

export default Nav;
