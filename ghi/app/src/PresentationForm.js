import React, {useEffect, useState} from 'react';


function PresentationForm(props) {
    const [presentations, setPresentations] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitleName] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value
        setPresenterName(value)
    }

    const handleEmailChange = (event) => {
        const value = event.target.value
        setPresenterEmail(value)
    }

    const handleCompanyChange = (event) => {
        const value = event.target.value
        setCompanyName(value)
    }

    const handleTitleChange = (event) =>  {
        const value = event.target.value
        setTitleName(value)
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value
        setSynopsis(value)
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        console.log(data)

        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation)

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitleName('');
            setSynopsis('');
            setConference('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setPresentations(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {presentations.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default PresentationForm
